/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello;

import it.unibas.printspooler.Costanti;
import it.unibas.printspooler.modello.logic.state.BusyState;
import it.unibas.printspooler.modello.logic.state.ErrorState;
import it.unibas.printspooler.modello.logic.state.FreeState;
import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.modello.operations.Operation;
import java.util.Calendar;
import java.util.GregorianCalendar;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class TestPrintSpooler {

    private PrintSpooler spooler;

    @Before
    public void initBeans() {
        spooler = new PrintSpooler();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSpoolerNull() {
        spooler.doPrint(null);
    }

    @Test
    public void testSpooilerFree() {

        spooler.setState(new FreeState());

        Operation newOperation = new Operation();
        newOperation.setCreationDate(new GregorianCalendar());

        TestCase.assertEquals(spooler.doPrint(newOperation), "Instered operation into wait queue");
        TestCase.assertEquals(spooler.getState().getDescriptionState(), Costanti.State.STATE_BUSY);
    }

    @Test
    public void testSpooilerBusyOneChangeState() {

        spooler.setState(new BusyState());

        Operation newOperation = new Operation();
        newOperation.setCreationDate(new GregorianCalendar());

        TestCase.assertEquals(spooler.doPrint(newOperation), "Instered operation into wait queue");
        TestCase.assertEquals(spooler.getState().getDescriptionState(), Costanti.State.STATE_BUSY);
    }

    @Test
    public void testSpooilerBusyTwoo() {

        Operation lastOperation = new Operation();

        GregorianCalendar lastOperationDate = new GregorianCalendar();

        GregorianCalendar fakeDate = new GregorianCalendar(
                lastOperationDate.get(Calendar.YEAR),
                lastOperationDate.get(Calendar.MONTH),
                lastOperationDate.get(Calendar.DAY_OF_MONTH) - 1);

        lastOperation.setCreationDate(fakeDate);
        
        spooler.getWaitOperation().add(lastOperation);

        spooler.setState(new BusyState());

        Operation newOperation = new Operation();
        newOperation.setCreationDate(new GregorianCalendar());

        TestCase.assertEquals(spooler.doPrint(newOperation), "The operation can be performed");
        TestCase.assertEquals(spooler.getState().getDescriptionState(), Costanti.State.STATE_BUSY);
    }
    
    @Test
    public void testSpooilerBusyThree() {

        spooler.setState(new BusyState());

        Operation newOperation = new Operation();
        newOperation.setCreationDate(new GregorianCalendar());

        TestCase.assertEquals(spooler.doPrint(newOperation), "Instered operation into wait queue");
        TestCase.assertEquals(spooler.getState().getDescriptionState(), Costanti.State.STATE_BUSY);
        TestCase.assertEquals(spooler.getWaitOperation().size(), 1);
    }
    
    @Test
    public void testSpooilerBusyFour() {

        spooler.setState(new ErrorState());

        Operation newOperation = new Operation();
        newOperation.setCreationDate(new GregorianCalendar());

        TestCase.assertEquals(spooler.doPrint(newOperation), "This operation can not be performed because the state of the print is ERROR");
        TestCase.assertEquals(spooler.getState().getDescriptionState(), Costanti.State.STATE_ERROR);
        TestCase.assertEquals(spooler.getWaitOperation().size(), 1);
    }

}
