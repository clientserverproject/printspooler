/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.vista;

import it.unibas.printspooler.modello.operations.Operation;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean
@ViewScoped
public class WaitOperationView {
    
    private List<Operation> waitOperations = new ArrayList<>();
    
    //Input propriety
    private Operation operation = new Operation();
    
    private String path;
    private float dimension;
    
    //Getter and Setter
    public List<Operation> getWaitOperations() {
        return waitOperations;
    }

    public void setWaitOperations(List<Operation> waitOperations) {
        this.waitOperations = waitOperations;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public float getDimension() {
        return dimension;
    }

    public void setDimension(float dimension) {
        this.dimension = dimension;
    }
    
    
    
    
}
