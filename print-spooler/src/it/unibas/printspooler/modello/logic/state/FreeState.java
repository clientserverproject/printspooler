/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello.logic.state;

import it.unibas.printspooler.Costanti;
import it.unibas.printspooler.modello.operations.Operation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class FreeState extends AbstractSpoolerState {
    
    private final Log LOGGER = LogFactory.getLog(FreeState.class);

    public FreeState() {
        super(Costanti.State.STATE_FREE);
    }

    @Override
    public boolean isBusy() {
        return false;
    }

    @Override
    public boolean isError() {
        return false;
    }

    @Override
    public boolean isFree() {
        return true;
    }

    @Override
    public String getDescriptionState() {
        return super.getState();
    }

    @Override
    public String hasPrintable(PrintSpooler spooler, Operation newOperation) {
        super.hasPrintable(spooler, newOperation);
        
        if(spooler.getWaitOperation().isEmpty()){
            LOGGER.debug("The wait queue operation is empty");
            spooler.addWaitOperation(newOperation);
            spooler.setState(new BusyState()); 
            return "Instered operation into wait queue"; //TODO is correct?;

        }
        LOGGER.error("Illegal state");
        throw new IllegalArgumentException("illegal state into free state");
    }
    
    
}
