/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello.logic.state;

import it.unibas.printspooler.modello.operations.Operation;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@Entity(name = "printsSpooler")
public class PrintSpooler implements Serializable{
    
    private final Log LOGGER = LogFactory.getLog(PrintSpooler.class);
    
    private Long id;
    private List<Operation> waitOperation = new ArrayList<>();
    
    private ISpoolerState state;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Transient
    public ISpoolerState getState() {
        return state;
    }

    public void setState(ISpoolerState state) {
        this.state = state;
    }
    
    
    @OneToMany(mappedBy = "printSpoiler", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    public List<Operation> getWaitOperation() {
        return waitOperation;
    }

    public void setWaitOperation(List<Operation> waitOperation) {
        this.waitOperation = waitOperation;
    }

    public void addWaitOperation(Operation operation){
        if(operation == null){
            throw new IllegalArgumentException("Operation null");
        }
        this.waitOperation.add(operation);
        operation.setPrintSpoiler(this);
    }
    
    public void removeWaitOperation(Operation operation){
        if(operation == null){
            throw new IllegalArgumentException("Operation null");
        }
        this.waitOperation.remove(operation);
        operation.setPrintSpoiler(null);
    }
   
    public String doPrint(Operation operation){
        if(operation == null){
            LOGGER.error("Operation null");
            throw new IllegalArgumentException("Operation null");
        }
        
        return state.hasPrintable(this, operation);
    }
    
    
}
