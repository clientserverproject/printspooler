/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello.logic.state;

import it.unibas.printspooler.modello.operations.Operation;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public interface ISpoolerState {
    
    String getDescriptionState();
    
    boolean isFree();
    boolean isError();
    boolean isBusy();
    
    String hasPrintable(PrintSpooler spooler, Operation newOperation);
    
}
