package it.unibas.printspooler.modello;

import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class Modello implements Serializable {
    
    private Utente utente;
    private PrintSpooler spooler;
    
    //Getter and setter
    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public PrintSpooler getSpooler() {
        return spooler;
    }

    public void setSpooler(PrintSpooler spooler) {
        this.spooler = spooler;
    }
    
    

}


