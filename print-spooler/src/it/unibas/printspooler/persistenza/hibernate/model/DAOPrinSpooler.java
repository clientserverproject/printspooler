/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.persistenza.hibernate.model;

import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.persistenza.IDAOPrintSpooler;
import it.unibas.printspooler.persistenza.hibernate.DAOGenericoHibernate;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean(eager = true)
@ApplicationScoped
public class DAOPrinSpooler extends DAOGenericoHibernate<PrintSpooler> implements IDAOPrintSpooler{
    
    public DAOPrinSpooler() {
        super(PrintSpooler.class);
    }
}
