/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.persistenza;

import it.unibas.printspooler.eccezioni.DAOException;
import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.modello.operations.Operation;
import java.util.List;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public interface IDAOOperations extends IDAOGenerico<Operation>{
    
    List<Operation> findSpecificOperations(PrintSpooler spooler, String esit) throws DAOException;
}
