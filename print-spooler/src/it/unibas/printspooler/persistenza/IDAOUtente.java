package it.unibas.printspooler.persistenza;

import it.unibas.printspooler.eccezioni.DAOException;
import it.unibas.printspooler.modello.Utente;

public interface IDAOUtente extends IDAOGenerico<Utente> {
     
    public Utente findByNomeUtente(String nomeUtente) throws DAOException;

    public void save(Utente utente) throws DAOException;

    public void delete(Utente utente) throws DAOException;

}
