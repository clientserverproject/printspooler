/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.controllo.actions;

import it.unibas.printspooler.Costanti;
import it.unibas.printspooler.eccezioni.DAOException;
import it.unibas.printspooler.modello.Modello;
import it.unibas.printspooler.modello.logic.state.BusyState;
import it.unibas.printspooler.modello.logic.state.FreeState;
import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.modello.operations.Operation;
import it.unibas.printspooler.persistenza.IDAOOperations;
import it.unibas.printspooler.persistenza.IDAOPrintSpooler;
import it.unibas.printspooler.vista.WaitOperationView;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean
@ViewScoped
public class ActionDisplaysWaitOperations {

    private final transient Log LOGGER = LogFactory.getLog(ActionDisplaysWaitOperations.class);

    @ManagedProperty(value = "#{dAOOperations}")
    private IDAOOperations daoOperations;
    @ManagedProperty(value = "#{dAOPrinSpooler}")
    private IDAOPrintSpooler daoHibernateSpooler;
    @ManagedProperty(value = "#{waitOperationView}")
    private WaitOperationView view;
    @ManagedProperty(value = "#{modello}")
    private Modello modello;

    public void initBeans() {
        try {
            PrintSpooler spooler = modello.getSpooler();
            if (modello.getSpooler() == null) {
                spooler = daoHibernateSpooler.findAll().get(0);
            }

            if (spooler != null) {
                if (spooler.getState() == null) {
                    if (spooler.getWaitOperation().isEmpty()) {
                        spooler.setState(new FreeState());
                    } else {
                        spooler.setState(new BusyState());
                    }
                    modello.setSpooler(spooler);
                }
                /* exception generated 
                --------------------------
                Exception generated is: org.hibernate.QueryException: could not resolve property:
                --------------------------
                daoHibernateSpooler.makePersistent(spooler);
                List<Operation> operations = daoOperations.findSpecificOperations(spooler, Costanti.MessageStateOperation.MESSAGE_WAIT);
                view.setWaitOperations(operations);
                */
                view.setWaitOperations(filterTheListOperations(spooler.getWaitOperation()));
            }

        } catch (DAOException ex) {
            LOGGER.error("Exception generated is: " + ex.getLocalizedMessage());
        }
    }

    public void addOperation() {

        String path = view.getPath();
        float dimension = view.getDimension();

        //Operation operation = view.getOperation();
        Operation operation = new Operation();
        operation.setPathFile(path);
        operation.setDimensionFile(dimension);
        operation.setCreationDate(new GregorianCalendar());

        LOGGER.debug("***** The value path is: " + operation.getPathFile() + " ******");
        LOGGER.debug("***** The value dimension is: " + operation.getDimensionFile() + " ******");

        PrintSpooler spooler = modello.getSpooler();

        try {
            if (spooler != null) {

                daoOperations.makePersistent(operation);
                daoHibernateSpooler.makePersistent(spooler); //reattach the object to the session

                String message = "File added to wait queue";

                if (spooler.getState().getDescriptionState().equals(Costanti.State.STATE_BUSY)) {
                    operation.setDescriptionState(Costanti.MessageStateOperation.MESSAGE_WAIT);

                    spooler.addWaitOperation(operation);
                } else {
                    message = spooler.doPrint(operation);
                }

                daoHibernateSpooler.makePersistent(spooler); //Make change persistent
                
                LOGGER.debug("\n******* Dimension wait queue " + spooler.getWaitOperation().size() + " **********\n");
                FacesContext.getCurrentInstance().addMessage(":growl",
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "INFO message",
                                message));
            }

        } catch (DAOException ex) {
            LOGGER.error("Exception generated is: " + ex.getLocalizedMessage());
        }

    }
    
    private List<Operation> filterTheListOperations(List<Operation> operations) {
        if(operations == null){
            throw new IllegalArgumentException("List null");
        }
        
        List<Operation> results = new ArrayList<>();
        for(Operation operation: operations){
            if(!operation.getDescriptionState().equals(Costanti.MessageStateOperation.MESSAGE_COMPLETE)){
                results.add(operation);
            }
        }
        LOGGER.debug(" ********* The results size is: " + results.size() +" ************");
        return results;
    }

    //Getter and setter
    public WaitOperationView getView() {
        return view;
    }

    public void setView(WaitOperationView view) {
        this.view = view;
    }

    public IDAOPrintSpooler getDaoHibernateSpooler() {
        return daoHibernateSpooler;
    }

    public void setDaoHibernateSpooler(IDAOPrintSpooler daoHibernateSpooler) {
        this.daoHibernateSpooler = daoHibernateSpooler;
    }

    public Modello getModello() {
        return modello;
    }

    public void setModello(Modello modello) {
        this.modello = modello;
    }

    public IDAOOperations getDaoOperations() {
        return daoOperations;
    }

    public void setDaoOperations(IDAOOperations daoOperations) {
        this.daoOperations = daoOperations;
    }

}
