/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.controllo.actions;

import it.unibas.printspooler.Costanti;
import it.unibas.printspooler.eccezioni.DAOException;
import it.unibas.printspooler.modello.Modello;
import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.modello.operations.Operation;
import it.unibas.printspooler.persistenza.IDAOPrintSpooler;
import it.unibas.printspooler.vista.ViewSuccessOperations;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean
@ViewScoped
public class ActionLoadSuccessOperation {
    
    private transient final Log LOGGER = LogFactory.getLog(ActionLoadSuccessOperation.class);

    @ManagedProperty(value = "#{viewSuccessOperations}")
    private ViewSuccessOperations view;
    @ManagedProperty(value = "#{modello}")
    private Modello modello;
    @ManagedProperty(value = "#{dAOPrinSpooler}")
    private IDAOPrintSpooler daoHibernateSpooler;

    public void initBeans() {
        try {
            PrintSpooler spooler = daoHibernateSpooler.findAll().get(0);
            List<Operation> operationsFilter = filteTheListOperations(spooler.getWaitOperation());
            if (spooler != null) {
                view.setSuccessOperation(operationsFilter);
            }
        } catch (DAOException ex) {
            ex.printStackTrace();
        }
    }
    
    private List<Operation> filteTheListOperations(List<Operation> waitOperation) {
        if(waitOperation == null){
            throw new IllegalArgumentException("List null");
        }
        
        List<Operation> results = new ArrayList<>();
        for(Operation operation: waitOperation){
            if(operation.getDescriptionState().equals(Costanti.MessageStateOperation.MESSAGE_COMPLETE)){
                results.add(operation);
            }
        }
        LOGGER.debug(" ********* The results size is: " + results.size() +" ************");
        return results;
    }


    //getter and setter
    public ViewSuccessOperations getView() {
        return view;
    }

    public void setView(ViewSuccessOperations view) {
        this.view = view;
    }

    public Modello getModello() {
        return modello;
    }

    public void setModello(Modello modello) {
        this.modello = modello;
    }

    public IDAOPrintSpooler getDaoHibernateSpooler() {
        return daoHibernateSpooler;
    }

    public void setDaoHibernateSpooler(IDAOPrintSpooler daoHibernateSpooler) {
        this.daoHibernateSpooler = daoHibernateSpooler;
    }

    
}
