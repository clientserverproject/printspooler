/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.controllo.converters;

import java.text.DateFormat;
import java.util.Calendar;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@FacesConverter("it.unibas.printspooler.controllo.converters.DateConverter")
public class DateConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        return getAsString(fc, uic, fc);
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if(o == null){
            return null;
        }
        
        Calendar calendar = (Calendar) o;
        
        DateFormat formatDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
        return formatDate.format(calendar.getTime());
    }
    
    
}
