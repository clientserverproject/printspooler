/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.vista;

import it.unibas.printspooler.modello.operations.Operation;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean
@ViewScoped
public class ViewSuccessOperations {
    
    private List<Operation> successOperation = new ArrayList<>();
    
    //gette and setter 
    public List<Operation> getSuccessOperation() {
        return successOperation;
    }

    public void setSuccessOperation(List<Operation> successOperation) {
        this.successOperation = successOperation;
    }
    
}
