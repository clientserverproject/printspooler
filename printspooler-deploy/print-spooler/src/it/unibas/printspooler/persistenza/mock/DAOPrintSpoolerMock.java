/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.persistenza.mock;

import it.unibas.printspooler.Costanti;
import it.unibas.printspooler.eccezioni.DAOException;
import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.modello.operations.Operation;
import it.unibas.printspooler.persistenza.IDAOPrintSpooler;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean(eager = true)
@ApplicationScoped
public class DAOPrintSpoolerMock implements IDAOPrintSpooler{

    private PrintSpooler printSpooler = new PrintSpooler();

    public DAOPrintSpoolerMock() {
        
        Operation firstWaitOperation = new Operation();
        firstWaitOperation.setCreationDate(new GregorianCalendar(1996, Calendar.JUNE, 24));
        firstWaitOperation.setDimensionFile(30);
        firstWaitOperation.setDescriptionState(Costanti.MessageStateOperation.MESSAGE_WAIT);
        firstWaitOperation.setPathFile("/home/vincenzo/esame-psa-5072019.pdf");
        
        Operation secondWaitOperation = new Operation();
        secondWaitOperation.setCreationDate(new GregorianCalendar(1998, Calendar.AUGUST, 3));
        secondWaitOperation.setDimensionFile(100);
        secondWaitOperation.setDescriptionState(Costanti.MessageStateOperation.MESSAGE_WAIT);
        secondWaitOperation.setPathFile("/home/vincenzo/manuale-primefesis6.pdf");
        
        Operation thirdWaitOperation = new Operation();
        thirdWaitOperation.setCreationDate(new GregorianCalendar(2019, Calendar.JUNE, 23));
        thirdWaitOperation.setDimensionFile(100);
        thirdWaitOperation.setDescriptionState(Costanti.MessageStateOperation.MESSAGE_WAIT);
        thirdWaitOperation.setPathFile("/home/vincenzo/media/usbtmp/risultati-esame-psa.pdf");
        
        printSpooler.addWaitOperation(firstWaitOperation);
        printSpooler.addWaitOperation(secondWaitOperation);
        printSpooler.addWaitOperation(thirdWaitOperation);
        
    }
    
    
    
    @Override
    public PrintSpooler findById(Long id, boolean lock) throws DAOException {
        return printSpooler;
    }

    @Override
    public List<PrintSpooler> findAll() throws DAOException {
        List<PrintSpooler> spoolers = new ArrayList<>();
        spoolers.add(printSpooler);
        return spoolers;
    }

    @Override
    public List<PrintSpooler> findAll(int offset, int limite) throws DAOException {
       List<PrintSpooler> spoolers = new ArrayList<>();
        spoolers.add(printSpooler);
        return spoolers;
    }

    @Override
    public PrintSpooler makePersistent(PrintSpooler entity) throws DAOException {
        printSpooler = entity;
        return printSpooler;
    }

    @Override
    public void makeTransient(PrintSpooler entity) throws DAOException {
        printSpooler = null;
    }

    @Override
    public void lock(PrintSpooler entity) throws DAOException {
        //do nothing
    }

    @Override
    public PrintSpooler update(PrintSpooler entity) throws DAOException {
        return printSpooler;
    }
    
}
