/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.persistenza.hibernate.model;

import it.unibas.printspooler.eccezioni.DAOException;
import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.modello.operations.Operation;
import it.unibas.printspooler.persistenza.IDAOOperations;
import it.unibas.printspooler.persistenza.hibernate.DAOGenericoHibernate;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean(eager = true)
@ApplicationScoped
public class DAOOperations extends DAOGenericoHibernate<Operation> implements IDAOOperations{
    
    public DAOOperations() {
        super(Operation.class);
    }
    
    //TODO this sholud be resolved the global query into display
    public List<Operation> findSpecificOperations(PrintSpooler spooler, String esit) throws DAOException{
        return findByCriteria(Restrictions.eq("printspoiler_id", spooler.getId()), 
                                Restrictions.eq("descriptionstate", esit));
    }
    
}
