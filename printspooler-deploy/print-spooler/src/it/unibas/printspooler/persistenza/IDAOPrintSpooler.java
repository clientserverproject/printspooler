/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.persistenza;

import it.unibas.printspooler.modello.logic.state.PrintSpooler;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public interface IDAOPrintSpooler extends IDAOGenerico<PrintSpooler>{
    
}
