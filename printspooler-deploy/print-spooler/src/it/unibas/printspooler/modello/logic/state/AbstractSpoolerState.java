/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello.logic.state;

import it.unibas.printspooler.modello.operations.Operation;
import java.io.Serializable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public abstract class AbstractSpoolerState implements ISpoolerState, Serializable{
    
    private static final Log LOGGER = LogFactory.getLog(AbstractSpoolerState.class);
    
    private String state;

    public AbstractSpoolerState(String state) {
        this.state = state;
    }

    public AbstractSpoolerState() {}

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String hasPrintable(PrintSpooler spooler, Operation newOperation) {
        if(spooler == null || newOperation == null){
            LOGGER.error("Paramiter input null");
            throw new IllegalArgumentException("Paramiter input null");
        }
        return "";
    }
    
    
    
}
