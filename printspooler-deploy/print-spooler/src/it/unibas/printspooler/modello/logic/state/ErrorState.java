/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello.logic.state;

import it.unibas.printspooler.Costanti;
import it.unibas.printspooler.modello.operations.Operation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class ErrorState extends AbstractSpoolerState {

    private static final Log LOGGER = LogFactory.getLog(ErrorState.class);

    public ErrorState() {
        super(Costanti.State.STATE_ERROR);
    }

    @Override
    public boolean isBusy() {
        return false;
    }

    @Override
    public boolean isError() {
        return true;
    }

    @Override
    public boolean isFree() {
        return false;
    }

    @Override
    public String getDescriptionState() {
        return super.getState();
    }

    @Override
    public String hasPrintable(PrintSpooler spooler, Operation newOperation) {
        super.hasPrintable(spooler, newOperation);

        LOGGER.warn("This operation can not be performed because the state of the print is ERROR");
        newOperation.setDescriptionState(Costanti.MessageStateOperation.MESSAGE_ERROR);
        if (!spooler.getWaitOperation().contains(newOperation)) {
            spooler.addWaitOperation(newOperation);

        }
        return "This operation can not be performed because the state of the print is ERROR";
    }

}
