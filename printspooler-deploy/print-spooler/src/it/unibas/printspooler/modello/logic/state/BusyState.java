/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello.logic.state;

import it.unibas.printspooler.Costanti;
import it.unibas.printspooler.modello.operations.Operation;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class BusyState extends AbstractSpoolerState {

    private final Log LOGGER = LogFactory.getLog(BusyState.class);
    static final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs

    public BusyState() {
        super(Costanti.State.STATE_BUSY);
    }

    @Override
    public boolean isBusy() {
        return true;
    }

    @Override
    public boolean isError() {
        return false;
    }

    @Override
    public boolean isFree() {
        return false;
    }

    @Override
    public String getDescriptionState() {
        return super.getState();
    }

    @Override
    //TODO refactoring with DATE.after Date.before ??
    public String hasPrintable(PrintSpooler spooler, Operation newOperation) {
        super.hasPrintable(spooler, newOperation);

        if (spooler.getWaitOperation().isEmpty()) {
            LOGGER.warn("The state of the spooler is into busy but the state "
                    + "of the spooler must be free because the wait queue is empty");
            spooler.setState(new FreeState());
            return spooler.doPrint(newOperation);
        }

        Operation lastOperation = spooler.getWaitOperation().get(spooler.getWaitOperation().size() - 1);

        if (lastOperation.equals(newOperation)) {
            LOGGER.debug("Last opration is equals to new operation");
            LOGGER.debug("The wait queue is empty and the state is busy");
            LOGGER.debug("Now the operation can be performed and the state is free");
            newOperation.setDescriptionState(Costanti.MessageStateOperation.MESSAGE_COMPLETE);
            if (spooler.getWaitOperation().isEmpty()) {
                LOGGER.debug("The wait queue is empty so setting the state to FreeState");
                spooler.setState(new FreeState());
            }
            return "The operation can be performed";
        }

        Calendar dateLastOperation = lastOperation.getCreationDate();

        Date newOpDate = newOperation.getCreationDate().getTime();
        Date lastOpDate = lastOperation.getCreationDate().getTime();

        long t = dateLastOperation.getTimeInMillis();
        Date afterAddingTenMins = new Date(t + (((int)lastOperation.getDimensionFile() / 60) * ONE_MINUTE_IN_MILLIS));

        if (newOpDate.after(afterAddingTenMins)) {
            //TODO this work but not work well for me
            LOGGER.debug("The operation can be performed");
            return setNewOperationOK(spooler, newOperation);
        } else {
            return "The operation can not be performed";
        }

        //TODO This code not work with same Date and time different
        /*
        //Year test
        int yearOpertion = newOperation.getCreationDate().get(Calendar.YEAR);
        LOGGER.debug("Year operation: " + yearOpertion);
        int yearAttual = dateLastOperation.get(Calendar.YEAR);
        LOGGER.debug("Attual year: " + yearAttual);

        if (yearOpertion > yearAttual) {
            LOGGER.debug("The operation can be performed");
            return setNewOperationOK(spooler, newOperation);
        }

        //Month test
        int monthOpertion = newOperation.getCreationDate().get(Calendar.MONTH);
        LOGGER.debug("Month operation: " + monthOpertion);
        int monthAttual = dateLastOperation.get(Calendar.MONTH);
        LOGGER.debug("Attual month: " + monthAttual);

        if (monthOpertion > monthAttual) {
            LOGGER.debug("The operation can be performed");
            return setNewOperationOK(spooler, newOperation);
        }

        //Day test
        int dayOpertion = newOperation.getCreationDate().get(Calendar.DAY_OF_MONTH);
        LOGGER.debug("Day operation: " + dayOpertion);
        int dayAttual = dateLastOperation.get(Calendar.DAY_OF_MONTH);
        LOGGER.debug("Attual day: " + dayAttual);

        if (dayOpertion > dayAttual) {
            LOGGER.debug("The operation can be performed");
            return setNewOperationOK(spooler, newOperation);
        }

        //Time test
        int timeStart = newOperation.getCreationDate().get(Calendar.HOUR);
        LOGGER.debug("Time start operation: " + timeStart);
        int attualTime = dateLastOperation.get(Calendar.HOUR);
        LOGGER.debug("Attual time: " + attualTime);

        float differenceHours = (((float) timeStart) - ((float) attualTime));

        if (differenceHours > newOperation.getDimensionFile()) {
            LOGGER.debug("The operation can be performed");
            return setNewOperationOK(spooler, newOperation);
        }
        LOGGER.debug("The operation can not be performed");
        if (!spooler.getWaitOperation().contains(newOperation)) {
            LOGGER.warn("The wait operation isn't into wait queue, I'm adding this");
            newOperation.setDescriptionState("In coda");

            spooler.addWaitOperation(newOperation);

        }
        return "The operation can not be performed";
         */
    }

    /**
     * Service method
     *
     * @param spooler
     * @param newOperation
     * @return String and this string is a esit operation
     */
    private String setNewOperationOK(PrintSpooler spooler, Operation newOperation) {
        if (spooler.getWaitOperation().contains(newOperation)) {
            LOGGER.debug("The new operation is contains into wait queue");
            newOperation.setDescriptionState(Costanti.MessageStateOperation.MESSAGE_COMPLETE);
            return "The operation can be performed";
        }
        LOGGER.warn("The new operation isn't contains into wait queue");
        newOperation.setDescriptionState(Costanti.MessageStateOperation.MESSAGE_COMPLETE);
        return "The operation can be performed";
    }

}
