/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello.operations;

import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@Entity(name = "operations")
public class Operation implements Serializable {

    private Calendar creationDate;
    private Calendar executionDate;
    private String pathFile;
    private float dimensionFile;
    private String descriptionState;

    //for hibernate mapping
    private Long id;
    private PrintSpooler printSpoiler;

    @Temporal(TemporalType.TIMESTAMP)
    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Calendar getExecutionDate() {
        return executionDate;
    }

    public void setExecutionDate(Calendar executionDate) {
        this.executionDate = executionDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public float getDimensionFile() {
        return dimensionFile;
    }

    public void setDimensionFile(float dimensionFile) {
        this.dimensionFile = dimensionFile;
    }

    public String getDescriptionState() {
        return descriptionState;
    }

    public void setDescriptionState(String descriptionState) {
        this.descriptionState = descriptionState;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.creationDate);
        hash = 89 * hash + Objects.hashCode(this.pathFile);
        hash = 89 * hash + Float.floatToIntBits(this.dimensionFile);
        return hash;
    }

    @ManyToOne
    public PrintSpooler getPrintSpoiler() {
        return printSpoiler;
    }

    public void setPrintSpoiler(PrintSpooler printSpoiler) {
        this.printSpoiler = printSpoiler;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Operation other = (Operation) obj;
        if (Float.floatToIntBits(this.dimensionFile) != Float.floatToIntBits(other.dimensionFile)) {
            return false;
        }
        if (!Objects.equals(this.pathFile, other.pathFile)) {
            return false;
        }
        if (!Objects.equals(this.creationDate, other.creationDate)) {
            return false;
        }
        return true;
    }

}
