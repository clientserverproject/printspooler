/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.controllo.actions;

import it.unibas.printspooler.Costanti;
import it.unibas.printspooler.eccezioni.DAOException;
import it.unibas.printspooler.modello.Modello;
import it.unibas.printspooler.modello.logic.state.BusyState;
import it.unibas.printspooler.modello.logic.state.ErrorState;
import it.unibas.printspooler.modello.logic.state.FreeState;
import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.modello.operations.Operation;
import it.unibas.printspooler.persistenza.IDAOOperations;
import it.unibas.printspooler.persistenza.IDAOPrintSpooler;
import java.util.GregorianCalendar;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean
@ViewScoped
public class ActionPrintOperation {

    private final transient Log LOGGER = LogFactory.getLog(ActionPrintOperation.class);

    @ManagedProperty(value = "#{dAOOperations}")
    private IDAOOperations daoOperations;
    @ManagedProperty(value = "#{dAOPrinSpooler}")
    private IDAOPrintSpooler daoHibernateSpooler;
    @ManagedProperty(value = "#{modello}")
    private Modello modello;

    public void runOperationWithState(Operation operation) {
        if (operation == null) {
            throw new IllegalArgumentException("Operation null");
        }

        PrintSpooler spooler = modello.getSpooler();

        try {
            //reattac the object to the session
            //daoOperations.makePersistent(operation);
            daoHibernateSpooler.makePersistent(spooler);
            
            operation.setCreationDate(new GregorianCalendar());
            
            String message = spooler.doPrint(operation);
            LOGGER.debug("********* The esit of the executions state is: " + message + " ********");

            //Realineating change on the spooler
            daoHibernateSpooler.makePersistent(spooler);
            
            LOGGER.debug("\n******* Dimension wait queue " + spooler.getWaitOperation().size() + " **********\n");
            FacesContext.getCurrentInstance().addMessage(":growl",
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "INFO message",
                            message));

        } catch (DAOException ex) {
            LOGGER.error("Exception generated is: " + ex.getLocalizedMessage());
            ex.printStackTrace();
        }
    }

    public void disableSpooler() {
        PrintSpooler spooler = modello.getSpooler();

        spooler.setState(new ErrorState());
        
        FacesContext.getCurrentInstance().addMessage(":growl",
                new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "WARNING message",
                        "Spoler in stato erroneo"));
    }
    
    public void restoreSpooler() {
        PrintSpooler spooler = modello.getSpooler();
        
        String message = "Stato spooler in ";
        
        if(!spooler.getWaitOperation().isEmpty()){
            message += "attesa";
            spooler.setState(new FreeState());
        }else{
            message += "libero";
            spooler.setState(new BusyState());
        }
        
        FacesContext.getCurrentInstance().addMessage(":growl",
                new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "WARNING message",
                            message));
    }
    
    public boolean disableButtonRestoreError(){
        return !modello.getSpooler().getState().getDescriptionState().equals(Costanti.State.STATE_ERROR);
    }
    
    public boolean disableButtonSetError(){
        return modello.getSpooler().getState().getDescriptionState().equals(Costanti.State.STATE_ERROR);
    }

//getter and setter
    public IDAOOperations getDaoOperations() {
        return daoOperations;
    }

    public void setDaoOperations(IDAOOperations daoOperations) {
        this.daoOperations = daoOperations;
    }

    public Modello getModello() {
        return modello;
    }

    public void setModello(Modello modello) {
        this.modello = modello;
    }

    public IDAOPrintSpooler getDaoHibernateSpooler() {
        return daoHibernateSpooler;
    }

    public void setDaoHibernateSpooler(IDAOPrintSpooler daoHibernateSpooler) {
        this.daoHibernateSpooler = daoHibernateSpooler;
    }

}
