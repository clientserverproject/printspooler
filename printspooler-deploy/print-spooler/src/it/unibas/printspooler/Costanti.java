package it.unibas.printspooler;

public class Costanti {

    public static final String REDIRECT = "?faces-redirect=true&includeViewParams=true";

    public static class State {
           
        public static final String STATE_FREE = "Stat";
        public static final String STATE_BUSY = "busy";
        public static final String STATE_ERROR = "error";
    }
    
    public static class MessageStateOperation{
        
        public static final String MESSAGE_WAIT = "In coda";
        public static final String MESSAGE_ERROR = "Errore stampante";
        public static final String MESSAGE_COMPLETE = "Stampato";
    }

}
