
    create table operations (
        id int8 not null,
        creationDate timestamp,
        descriptionState varchar(255),
        dimensionFile float4 not null,
        executionDate timestamp,
        pathFile varchar(255),
        printSpoiler_id int8,
        primary key (id)
    );

    create table printsSpooler (
        id int8 not null,
        primary key (id)
    );

    create table utente (
        id int8 not null,
        attivo boolean not null,
        lastLogin timestamp,
        nome varchar(255),
        nomeUtente varchar(255),
        password varchar(255),
        ruolo varchar(255),
        primary key (id)
    );

    alter table utente 
        add constraint UK_7hipuu05v6vcqr7wbl8q7p4t2 unique (nomeUtente);

    alter table operations 
        add constraint FK_nvm1duth98ix70f58bxtwyry5 
        foreign key (printSpoiler_id) 
        references printsSpooler;

    create table hibernate_sequences (
         sequence_name varchar(255),
         sequence_next_hi_value int4 
    );
