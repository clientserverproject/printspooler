/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello;

import it.unibas.printspooler.modello.logic.state.BusyState;
import it.unibas.printspooler.modello.logic.state.ISpoolerState;
import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.modello.operations.Operation;
import java.util.Calendar;
import java.util.GregorianCalendar;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class TestBusyState {

    private ISpoolerState state;
    private Operation lastOperation;
    private PrintSpooler spooler;

    @Before
    public void initBeans() {
        state = new BusyState();
        lastOperation = new Operation();

        lastOperation.setCreationDate(new GregorianCalendar(2019, Calendar.JUNE, 24));

        spooler = new PrintSpooler();
        spooler.getWaitOperation().add(lastOperation);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStateOperationNull() {
        state.hasPrintable(null, null);
    }

    @Test
    public void testStateOne() {
        Assert.assertTrue(state.isBusy());
        Assert.assertFalse(state.isError());
        Assert.assertFalse(state.isFree());
    }

    @Test
    public void testStateTwoYearDifferent() {
        Calendar creationOperationDate = lastOperation.getCreationDate();
        GregorianCalendar attualDate = new GregorianCalendar(
                (creationOperationDate.get(Calendar.YEAR) + 1),
                creationOperationDate.get(Calendar.MONTH),
                creationOperationDate.get(Calendar.DAY_OF_MONTH));

        Operation operation = new Operation();
        operation.setDimensionFile(20);
        operation.setCreationDate(attualDate);
        TestCase.assertEquals(state.hasPrintable(spooler, operation), ("The operation can be performed"));
        
    }

    @Test
    public void testStateTwoMonthDifferent() {
        Calendar creationOperationDate = lastOperation.getCreationDate();
        GregorianCalendar attualDate = new GregorianCalendar(
                (creationOperationDate.get(Calendar.YEAR)),
                creationOperationDate.get(Calendar.MONTH) + 1,
                creationOperationDate.get(Calendar.DAY_OF_MONTH));

        Operation operation = new Operation();
        operation.setDimensionFile(20);
        operation.setCreationDate(attualDate);
        TestCase.assertEquals(state.hasPrintable(spooler, operation), ("The operation can be performed"));
    }

    @Test
    public void testStateTwoDayDifferent() {
        Calendar creationOperationDate = lastOperation.getCreationDate();
        GregorianCalendar attualDate = new GregorianCalendar(
                (creationOperationDate.get(Calendar.YEAR)),
                creationOperationDate.get(Calendar.MONTH),
                creationOperationDate.get(Calendar.DAY_OF_MONTH) + 1);

        Operation operation = new Operation();
        operation.setDimensionFile(20);
        operation.setCreationDate(attualDate);
        TestCase.assertEquals(state.hasPrintable(spooler, operation), ("The operation can be performed"));
    }

    /**
    @Test
    public void testStateTwoMinutesDifferentOne() {
        Calendar creationOperationDate = lastOperation.getCreationDate();
        GregorianCalendar attualDate = new GregorianCalendar(
                (creationOperationDate.get(Calendar.YEAR)),
                creationOperationDate.get(Calendar.MONTH),
                creationOperationDate.get(Calendar.DAY_OF_MONTH));

        Operation operation = new Operation();
        operation.setDimensionFile(1); //Zero because the time different is one second
        operation.setCreationDate(attualDate);
        TestCase.assertEquals(state.hasPrintable(spooler, operation), ("The operation can be performed"));
        TestCase.assertEquals(spooler.getSuccessulfuOperation().size(), 1);
        TestCase.assertEquals(spooler.getWaitOperation().size(), 1);
    }
*/
    @Test
    public void testStateTwoMinutesDifferentTwoo() {
        Calendar creationOperationDate = lastOperation.getCreationDate();
        GregorianCalendar attualDate = new GregorianCalendar(
                (creationOperationDate.get(Calendar.YEAR)),
                creationOperationDate.get(Calendar.MONTH),
                creationOperationDate.get(Calendar.DAY_OF_MONTH));

        Operation operation = new Operation();
        operation.setDimensionFile(30);
        operation.setCreationDate(attualDate);
        TestCase.assertEquals(state.hasPrintable(spooler, operation), ("The operation can not be performed"));
    }

}
