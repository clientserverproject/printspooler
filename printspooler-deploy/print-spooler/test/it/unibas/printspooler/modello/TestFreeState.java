/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello;

import it.unibas.printspooler.modello.logic.state.FreeState;
import it.unibas.printspooler.modello.logic.state.ISpoolerState;
import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.modello.operations.Operation;
import java.util.GregorianCalendar;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class TestFreeState {

    private ISpoolerState state;
    private PrintSpooler spooler;
    private Operation lastOperation;

    @Before
    public void initBeans() {
        state = new FreeState();
        spooler = new PrintSpooler();

        lastOperation = new Operation();
        lastOperation.setCreationDate(new GregorianCalendar());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFreeStateNull() {
        state.hasPrintable(null, null);
    }

    @Test
    public void testFreeStateOne() {

        Operation newOperation = new Operation();
        newOperation.setCreationDate(new GregorianCalendar());

        TestCase.assertEquals(state.hasPrintable(spooler, newOperation), "Instered operation into wait queue");
        TestCase.assertEquals(spooler.getWaitOperation().size(), 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFreeStateTwoo() {

        Operation newOperation = new Operation();
        newOperation.setCreationDate(new GregorianCalendar());
        
        lastOperation.setDimensionFile(20);

        spooler.getWaitOperation().add(lastOperation);

        state.hasPrintable(spooler, newOperation);
    }

}
