/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.printspooler.modello;

import it.unibas.printspooler.modello.logic.state.ErrorState;
import it.unibas.printspooler.modello.logic.state.ISpoolerState;
import it.unibas.printspooler.modello.logic.state.PrintSpooler;
import it.unibas.printspooler.modello.operations.Operation;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class TestErrorState {
    
    private PrintSpooler spooler;
    private ISpoolerState state;
    private Operation lastOperation;
            
    @Before
    public void initBeans(){
        spooler = new PrintSpooler();
        state = new ErrorState();
        lastOperation = new Operation();
        
        
        spooler.getWaitOperation().add(lastOperation);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testErrorStateNull(){
        state.hasPrintable(null, null);
    }
    
    public void testErrorStateOne(){
        TestCase.assertEquals(state.hasPrintable(spooler, lastOperation), 
                                "This operation can not be performed because the state of the print is ERROR");
        TestCase.assertEquals(spooler.getWaitOperation().size(), 1);
    }
}
